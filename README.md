# Dart Game App

This is a Dart scoring application built with Flutter, allowing users to play traditional dart games with various game modes and settings. The app supports multiple players and provides a digital display of scores, current round, and possible checkout ways.

## Features

- Multiple Game Modes: Play traditional 301 or 501 dart games.
- Multiple Game Settings: Choose between different game settings such as Straight Out, Double In, Double Out, Double In Double Out, and Master Out.
- Player Management: Add up to 8 players, reorder players, and delete players.
- Score Input: Use a dartboard or an on-screen keyboard to input scores.
- Checkout Suggestions: Displays possible checkout ways when a player is close to finishing the game.
- Save Player Profiles: Create and manage player profiles to save game history and stats.

## Screenshots

![Home Screen](screenshots/home_screen.png)
![Game Screen](screenshots/game_screen.png)

## Installation

1. **Clone the repository**

   ```sh
   git clone https://github.com/yourusername/dart-game-app.git
   cd dart-game-app
   ```

2. **Install dependencies**

   ```sh
   flutter pub get
   ```

3. **Run the application**
   ```sh
   flutter run
   ```

## Usage

1. **Home Screen**

   - Select game mode and game setting.
   - Add players by entering their names and clicking "Spieler hinzufügen". You can also drag players to reorder them or delete them using the trash icon.
   - Set the number of sets to win and legs per set using the provided controls.
   - Check the "Zufällige Reihenfolge" checkbox if you want to shuffle the player order randomly before starting the game.
   - Click "Spiel starten" to begin the game.

2. **Game Screen**
   - Input scores by either using the dartboard or the on-screen keyboard. Toggle between them using the "Tastatur nutzen" button.
   - View current scores, rounds, and possible checkout ways.
   - Use the "Letzten Wurf zurücksetzen" button to undo the last dart thrown.

## Contributing

Contributions are welcome! Please feel free to submit a Pull Request or open an issue.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Contact

For any questions or feedback, please reach out at [your-email@example.com].
