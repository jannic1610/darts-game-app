import 'package:Darts_Counter/widgets/drop_down_selector.dart';
import 'package:Darts_Counter/widgets/numbers_selector.dart';
import 'package:flutter/material.dart';
import '../controller/home_controller.dart';

class HomeView extends StatefulWidget {
  final HomeController controller;

  const HomeView(this.controller, {super.key});

  @override
  HomeViewState createState() => HomeViewState();
}

class HomeViewState extends State<HomeView> {
  @override
  void initState() {
    super.initState();
    widget.controller.loadInitialData().then((_) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Padding(
        padding: const EdgeInsets.only(top: 20.0),
        child: Container(
          padding: const EdgeInsets.all(16.0),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.black, Colors.grey[900]!],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              DropDownSelector(
                  label: 'Spielmodus',
                  value: widget.controller.model.selectedGameMode,
                  items: widget.controller.gameModes,
                  onChanged: (newValue) {
                    setState(() {
                      widget.controller.model.selectedGameMode = newValue!;
                    });
                  }),
              const SizedBox(height: 16),
              DropDownSelector(
                  label: 'Spiel Einstellung',
                  value: widget.controller.model.selectedGameSetting,
                  items: widget.controller.gameSettings,
                  onChanged: (newValue) {
                    setState(() {
                      widget.controller.model.selectedGameSetting = newValue!;
                    });
                  }),
              const SizedBox(height: 16),
              Row(
                children: [
                  Expanded(
                    child: NumbersSelector(
                        label: 'Anzahl der Sätze zum Gewinnen',
                        value: widget.controller.model.setsToWin,
                        min: 1,
                        max: 10,
                        onChanged: (value) {
                          setState(() {
                            widget.controller.model.setsToWin = value;
                          });
                        }),
                  ),
                  const SizedBox(width: 16),
                  Expanded(
                    child: NumbersSelector(
                        label: 'Anzahl der Legs pro Satz',
                        value: widget.controller.model.legsPerSet,
                        min: 1,
                        max: 10,
                        onChanged: (value) {
                          setState(() {
                            widget.controller.model.legsPerSet = value;
                          });
                        }),
                  ),
                ],
              ),
              const SizedBox(height: 16),
              CheckboxListTile(
                title: const Text('Zufällige Reihenfolge',
                    style: TextStyle(color: Colors.white)),
                value: widget.controller.model.randomOrder,
                onChanged: (bool? value) {
                  setState(() {
                    widget.controller.model.randomOrder = value!;
                  });
                },
                activeColor: Colors.red,
                checkColor: Colors.white,
              ),
              const SizedBox(height: 16),
              TextField(
                controller: widget.controller.playerController,
                decoration: const InputDecoration(
                  labelText: 'Spielername hinzufügen',
                  labelStyle: TextStyle(color: Colors.white),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.red),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                  ),
                ),
                style: const TextStyle(color: Colors.white),
                onSubmitted: (value) {
                  setState(() {
                    widget.controller.addPlayer(value);
                    widget.controller.playerController.clear();
                  });
                },
              ),
              const SizedBox(height: 8),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    widget.controller
                        .addPlayer(widget.controller.playerController.text);
                    widget.controller.playerController.clear();
                  });
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red[900],
                  textStyle: const TextStyle(color: Colors.white),
                ),
                child: const Text('Spieler hinzufügen'),
              ),
              const SizedBox(height: 16),
              SizedBox(
                height: 200,
                child: ReorderableListView(
                  onReorder: widget.controller.reorderPlayers,
                  children: List.generate(
                      widget.controller.model.players.length, (index) {
                    return ListTile(
                      key: ValueKey(widget.controller.model.players[index]),
                      title: Text(widget.controller.model.players[index],
                          style: const TextStyle(color: Colors.white)),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            icon: const Icon(Icons.remove_circle,
                                color: Colors.red),
                            onPressed: () => setState(() {
                              widget.controller.removePlayer(index);
                            }),
                          ),
                          const Icon(Icons.drag_handle, color: Colors.white),
                        ],
                      ),
                    );
                  }),
                ),
              ),
              const SizedBox(height: 16),
              TextField(
                controller: widget.controller.profileController,
                decoration: const InputDecoration(
                  labelText: 'Spielerprofil hinzufügen',
                  labelStyle: TextStyle(color: Colors.white),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.red),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                  ),
                ),
                style: const TextStyle(color: Colors.white),
                onSubmitted: (value) {
                  setState(() {
                    widget.controller.addPlayerProfile(value);
                    widget.controller.profileController.clear();
                  });
                },
              ),
              const SizedBox(height: 8),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    widget.controller.addPlayerProfile(
                        widget.controller.profileController.text);
                    widget.controller.profileController.clear();
                  });
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.green[700],
                  textStyle: const TextStyle(color: Colors.white),
                ),
                child: const Text('Profil hinzufügen'),
              ),
              const SizedBox(height: 16),
              SizedBox(
                height: 200,
                child: ListView.builder(
                  itemCount: widget.controller.model.playerProfiles.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(widget.controller.model.playerProfiles[index],
                          style: const TextStyle(color: Colors.white)),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            icon: const Icon(Icons.add_circle,
                                color: Colors.green),
                            onPressed: () => setState(() {
                              widget.controller.addPlayer(widget
                                  .controller.model.playerProfiles[index]);
                            }),
                          ),
                          IconButton(
                            icon: const Icon(Icons.delete, color: Colors.red),
                            onPressed: () => setState(() {
                              widget.controller
                                  .removePlayerProfile(index, context);
                            }),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
              const SizedBox(height: 16),
              ElevatedButton(
                onPressed: () {
                  widget.controller.startGame(context);
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red[900],
                  textStyle: const TextStyle(color: Colors.white),
                ),
                child: const Text('Spiel starten'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
