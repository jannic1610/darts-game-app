import 'package:flutter/material.dart';
import '../controller/main_controller.dart';
import '../widgets/dartboard_widget.dart';
import '../widgets/digital_display.dart';

class MainView extends StatefulWidget {
  final MainController controller;

  const MainView({super.key, required this.controller});

  @override
  MainViewState createState() => MainViewState();
}

class MainViewState extends State<MainView> {
  @override
  void initState() {
    super.initState();
    widget.controller.initializeGame();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Modus: ${widget.controller.model.gameMode} - ${widget.controller.model.gameSetting}',
              style: const TextStyle(
                fontFamily: 'Digital-7',
                color: Colors.red,
                fontSize: 30,
              ),
            ),
            const SizedBox(height: 20),
            Column(
              children: [
                for (int i = 0;
                    i < widget.controller.model.players.length;
                    i += 4)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      if (i < widget.controller.model.players.length)
                        DigitalDisplay(
                          label: widget.controller.model.players[i].name,
                          score: widget.controller.model.players[i].score,
                          isActive:
                              widget.controller.model.currentPlayerIndex == i,
                          legsWon: widget.controller.model.players[i].legsWon,
                          setsWon: widget.controller.model.players[i].setsWon,
                        ),
                      if (i + 1 < widget.controller.model.players.length) ...[
                        const SizedBox(width: 10),
                        DigitalDisplay(
                          label: widget.controller.model.players[i + 1].name,
                          score: widget.controller.model.players[i + 1].score,
                          isActive:
                              widget.controller.model.currentPlayerIndex ==
                                  i + 1,
                          legsWon:
                              widget.controller.model.players[i + 1].legsWon,
                          setsWon:
                              widget.controller.model.players[i + 1].setsWon,
                        ),
                      ],
                      if (i + 2 < widget.controller.model.players.length) ...[
                        const SizedBox(width: 10),
                        DigitalDisplay(
                          label: widget.controller.model.players[i + 2].name,
                          score: widget.controller.model.players[i + 2].score,
                          isActive:
                              widget.controller.model.currentPlayerIndex ==
                                  i + 2,
                          legsWon:
                              widget.controller.model.players[i + 2].legsWon,
                          setsWon:
                              widget.controller.model.players[i + 2].setsWon,
                        ),
                      ],
                      if (i + 3 < widget.controller.model.players.length) ...[
                        const SizedBox(width: 10),
                        DigitalDisplay(
                          label: widget.controller.model.players[i + 3].name,
                          score: widget.controller.model.players[i + 3].score,
                          isActive:
                              widget.controller.model.currentPlayerIndex ==
                                  i + 3,
                          legsWon:
                              widget.controller.model.players[i + 3].legsWon,
                          setsWon:
                              widget.controller.model.players[i + 3].setsWon,
                        ),
                      ],
                    ],
                  ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Container(
                      width: 200,
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.red),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Text(
                        'Runde: ${widget.controller.model.currentRound}',
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontFamily: 'Digital-7',
                          color: Colors.red,
                          fontSize: 35,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Container(
                      width: 200,
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.red),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Text(
                        'Wurf: ${widget.controller.model.currentRoundScore}',
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontFamily: 'Digital-7',
                          color: Colors.red,
                          fontSize: 35,
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(width: 20),
                Column(
                  children: [
                    ElevatedButton(
                      onPressed: () =>
                          setState(() => widget.controller.undoLastDart()),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.red,
                        textStyle: const TextStyle(fontSize: 30),
                      ),
                      child: const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text('Wurf zurücksetzen'),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 10),
            SizedBox(
              width: 250,
              height: 60,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: widget.controller.buildDartScores(),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                widget.controller.buildCheckoutWays(),
                const SizedBox(width: 10),
                ElevatedButton(
                  onPressed: () => setState(() => widget
                      .controller.useKeyboard = !widget.controller.useKeyboard),
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.red,
                    textStyle: const TextStyle(fontSize: 35),
                  ),
                  child: Text(widget.controller.useKeyboard
                      ? 'Dartboard nutzen'
                      : 'Tastatur nutzen'),
                ),
              ],
            ),
            widget.controller.useKeyboard
                ? widget.controller.buildKeyboard(context)
                : DartboardWidget(
                    key: widget.controller.dartboardKey,
                    onScoreChange: (int score, int multiplier) {
                      setState(() {
                        if (widget.controller.model.dartCount == 0) {
                          widget.controller.model.currentRoundScore = 0;
                          widget.controller.model.lastDarts.clear();
                        }
                        widget.controller
                            .updateScore(score, multiplier, context);
                      });
                    },
                  ),
          ],
        ),
      ),
    );
  }
}
