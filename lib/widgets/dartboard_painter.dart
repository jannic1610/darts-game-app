import 'dart:math' as math;
import 'package:flutter/material.dart';

class DartboardPainter extends CustomPainter {
  final List<int> scores = [
    6,
    10,
    15,
    2,
    17,
    3,
    19,
    7,
    16,
    8,
    11,
    14,
    9,
    12,
    5,
    20,
    1,
    18,
    4,
    13,
  ];

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()..style = PaintingStyle.fill;
    final center = Offset(size.width / 2, size.height / 2);
    final radius = math.min(size.width / 2.3, size.height / 2.3);

    final textPainter = TextPainter(
      textAlign: TextAlign.center,
      textDirection: TextDirection.ltr,
    );

    // Grundlegendes Dartboard
    for (int i = 0; i < 20; i++) {
      final angle = 18 * i - 9;
      final startAngle = angle * math.pi / 180;
      final endAngle = (angle + 18) * math.pi / 180;

      paint.color = i % 2 == 0 ? Colors.green.shade900 : Colors.red;
      canvas.drawArc(Rect.fromCircle(center: center, radius: radius),
          startAngle, endAngle - startAngle, true, paint);
    }

    // Doppelter Ring
    for (int i = 0; i < 20; i++) {
      final angle = 18 * i - 9;
      final startAngle = angle * math.pi / 180;
      final endAngle = (angle + 18) * math.pi / 180;

      paint.color =
          i % 2 == 0 ? Color.fromRGBO(246, 250, 189, 1) : Colors.black;
      canvas.drawArc(Rect.fromCircle(center: center, radius: radius * 0.9),
          startAngle, endAngle - startAngle, true, paint);
    }

    // Dreifacher Ring
    for (int i = 0; i < 20; i++) {
      final angle = 18 * i - 9;
      final startAngle = angle * math.pi / 180;
      final endAngle = (angle + 18) * math.pi / 180;

      paint.color = i % 2 == 0 ? Colors.green.shade900 : Colors.red;
      canvas.drawArc(Rect.fromCircle(center: center, radius: radius * 0.6),
          startAngle, endAngle - startAngle, true, paint);
    }

    // Inneres Single-Segment
    for (int i = 0; i < 20; i++) {
      final angle = 18 * i - 9;
      final startAngle = angle * math.pi / 180;
      final endAngle = (angle + 18) * math.pi / 180;

      paint.color =
          i % 2 == 0 ? Color.fromRGBO(246, 250, 189, 1) : Colors.black;
      canvas.drawArc(Rect.fromCircle(center: center, radius: radius * 0.5),
          startAngle, endAngle - startAngle, true, paint);
    }

    // Fügen Sie hier Text oder andere Elemente hinzu, falls benötigt
    for (int i = 0; i < 20; i++) {
      final angle = 18 * i - 9;
      final startAngle = angle * math.pi / 180;
      final endAngle = (angle + 18) * math.pi / 180;

      // Zeichne die Linien für die Sektoren
      paint.color = Colors.black;
      final xStart = center.dx + radius * math.cos(startAngle);
      final yStart = center.dy + radius * math.sin(startAngle);
      canvas.drawLine(center, Offset(xStart, yStart), paint);

      // Positioniere den Text
      final xText =
          center.dx + (radius + 20) * math.cos((startAngle + endAngle) / 2);
      final yText =
          center.dy + (radius + 20) * math.sin((startAngle + endAngle) / 2);
      textPainter.text = TextSpan(
        text: scores[i].toString(),
        style: const TextStyle(color: Colors.white, fontSize: 14),
      );
      textPainter.layout();
      textPainter.paint(
          canvas,
          Offset(
              xText - textPainter.width / 2, yText - textPainter.height / 2));
    }

    // Innerer Kreis
    paint.color = Colors.green.shade900;
    canvas.drawCircle(center, radius * 0.2, paint);

    // Bullseye als Punkt
    paint.color = Colors.red;
    canvas.drawCircle(center, radius * 0.075, paint);
    paint.color = Colors.black;
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true; // Always repaint to show new pins
  }
}
