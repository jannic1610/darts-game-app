import 'package:flutter/material.dart';

class DigitalDisplay extends StatelessWidget {
  final String label;
  final int score;
  final bool isActive;
  final int legsWon;
  final int setsWon;

  const DigitalDisplay({
    super.key,
    required this.label,
    required this.score,
    required this.isActive,
    required this.legsWon,
    required this.setsWon,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      width: MediaQuery.of(context).size.width *
          0.23, // Feste Breite für die Anzeige
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        border: Border.all(color: isActive ? Colors.green : Colors.red),
        borderRadius: BorderRadius.circular(8),
        color: Colors.black,
      ),
      child: Column(
        children: [
          Text(
            label,
            style: const TextStyle(
              fontFamily: 'Digital-7',
              color: Colors.red,
              fontSize: 30,
            ),
            overflow: TextOverflow.ellipsis, // Abkürzung langer Namen
          ),
          Text(
            score.toString(),
            style: const TextStyle(
              fontFamily: 'Digital-7',
              color: Colors.red,
              fontSize: 50,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Column(
                children: [
                  Text(
                    'Sätze: $setsWon',
                    style: const TextStyle(
                      fontFamily: 'Digital-7',
                      color: Colors.red,
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                    'Legs: $legsWon',
                    style: const TextStyle(
                      fontFamily: 'Digital-7',
                      color: Colors.red,
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
