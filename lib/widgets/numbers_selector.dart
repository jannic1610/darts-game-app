import 'package:flutter/material.dart';

class NumbersSelector extends StatelessWidget {
  const NumbersSelector({
    super.key,
    required this.label,
    required this.value,
    required this.min,
    required this.max,
    required this.onChanged,
  });

  final String label;
  final int value;
  final int min;
  final int max;
  final ValueChanged<int> onChanged;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            IconButton(
              icon: Icon(Icons.remove, color: Colors.red),
              onPressed: value > min ? () => onChanged(value - 1) : null,
            ),
            Text(
              value.toString(),
              style: TextStyle(fontSize: 24, color: Colors.white),
            ),
            IconButton(
              icon: Icon(Icons.add, color: Colors.red),
              onPressed: value < max ? () => onChanged(value + 1) : null,
            ),
          ],
        ),
      ],
    );
  }
}
