import 'dart:math' as math;
import 'package:Darts_Counter/widgets/dartboard_painter.dart';
import 'package:flutter/material.dart';

class DartboardWidget extends StatefulWidget {
  final Function(int, int) onScoreChange;

  const DartboardWidget({required this.onScoreChange, Key? key})
      : super(key: key);

  @override
  DartboardWidgetState createState() => DartboardWidgetState();
}

class DartboardWidgetState extends State<DartboardWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (TapDownDetails details) {
        final RenderBox renderBox = context.findRenderObject() as RenderBox;
        final offset = renderBox.globalToLocal(details.globalPosition);
        final score = calculateScore(offset, renderBox.size);
        widget.onScoreChange(score['score']!, score['multiplier']!);
      },
      child: CustomPaint(
        painter: DartboardPainter(),
        child: Container(
          height: 800,
          width: 800,
          decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
        ),
      ),
    );
  }

  Map<String, int> calculateScore(Offset offset, Size size) {
    final center = Offset(size.width / 2, size.height / 2);
    final dx = offset.dx - center.dx;
    final dy = offset.dy - center.dy;
    final radius = math.sqrt(dx * dx + dy * dy);
    final angle = (math.atan2(dy, dx) * 180 / math.pi + 360) % 360;

    final correctedAngle =
        (angle + 9 + 360) % 360; // Korrektur für die Ausrichtung des Dartboards
    const segmentAngle = 18; // 360 Grad geteilt durch 20 Segmente
    final segmentIndex = (correctedAngle / segmentAngle).floor() % 20;

    final List<int> scores = [
      6,
      10,
      15,
      2,
      17,
      3,
      19,
      7,
      16,
      8,
      11,
      14,
      9,
      12,
      5,
      20,
      1,
      18,
      4,
      13,
    ];

    final double normalizedRadius = radius /
        (math.min(size.width / 2.3,
            size.height / 2.3)); // Normalisierung auf den Radius

    int multiplier = 1;

    if (normalizedRadius < 0.075) {
      return {'score': 25, 'multiplier': 2}; // Bullseye
    } else if (normalizedRadius < 0.2) {
      return {'score': 25, 'multiplier': 1}; // Inner Circle
    } else if (normalizedRadius >= 0.5 && normalizedRadius < 0.6) {
      multiplier = 3; // Triple Ring
    } else if (normalizedRadius >= 0.9 && normalizedRadius <= 1.0) {
      multiplier = 2; // Double Ring
    } else if (normalizedRadius > 1) {
      return {'score': 0, 'multiplier': 1}; // Bereich außerhalb des Dartboards
    }

    return {
      'score': scores[segmentIndex],
      'multiplier': multiplier
    }; // Normaler Bereich
  }
}
