// controller/main_controller.dart
import 'package:Darts_Counter/widgets/dartboard_widget.dart';
import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';
import '../model/main_model.dart';
import '../utils/checkouts.dart';

class MainController {
  final MainModel model;
  final AudioPlayer _audioPlayer = AudioPlayer();

  final GlobalKey<DartboardWidgetState> dartboardKey =
      GlobalKey<DartboardWidgetState>();
  bool useKeyboard = false;
  int multiplier = 1;

  MainController(this.model);

  int getScoreForGameMode(String gameMode) {
    switch (gameMode) {
      case '501':
        return 501;
      case '301':
        return 301;
      case '701':
        return 701;
      default:
        return 0; // Fallback, falls ein ungültiger Modus ausgewählt ist
    }
  }

  void initializeGame() {
    for (var player in model.players) {
      player.score = getScoreForGameMode(model.gameMode);
    }
  }

  bool isDoubleField(int score, int segmentMultiplier) {
    return segmentMultiplier == 2 || score == 50;
  }

  bool checkMasterOut(int score, int segmentMultiplier) {
    return segmentMultiplier == 2 || segmentMultiplier == 3 || score == 50;
  }

  bool checkValidScore(int score, int remainingScore, int segmentMultiplier) {
    if (remainingScore == 1 &&
        (model.gameSetting == 'Double Out' ||
            model.gameSetting == 'Double In Double Out' ||
            model.gameSetting == 'Master Out')) {
      return false;
    }

    if (model.gameSetting == 'Double In' &&
        !model.players[model.currentPlayerIndex].doubleInAchieved) {
      return isDoubleField(score, segmentMultiplier);
    } else if (model.gameSetting == 'Double Out' && remainingScore == 0) {
      return isDoubleField(score, segmentMultiplier);
    } else if (model.gameSetting == 'Double In Double Out') {
      if (!model.players[model.currentPlayerIndex].doubleInAchieved) {
        return isDoubleField(score, segmentMultiplier);
      } else if (remainingScore == 0) {
        return isDoubleField(score, segmentMultiplier);
      }
    } else if (model.gameSetting == 'Master Out' && remainingScore == 0) {
      return checkMasterOut(score, segmentMultiplier);
    }
    return true;
  }

  Future<void> play180Sound() async {
    await _audioPlayer.setSource(AssetSource('180.mp3'));
    await _audioPlayer.resume();
  }

  void updateScore(int newScore, int segmentMultiplier, BuildContext context) {
    int remainingScore = model.players[model.currentPlayerIndex].score -
        (newScore * segmentMultiplier);

    if (model.gameSetting == 'Double In' &&
        !model.players[model.currentPlayerIndex].doubleInAchieved) {
      if (isDoubleField(newScore, segmentMultiplier)) {
        model.players[model.currentPlayerIndex].doubleInAchieved = true;
      } else {
        addToHistory(0, 1, model.players[model.currentPlayerIndex].score);
        addLastDart(0, 1);
        model.dartCount++;
        if (model.dartCount >= 3) {
          switchPlayer();
        }
        return;
      }
    }

    if (!checkValidScore(newScore, remainingScore, segmentMultiplier)) {
      model.players[model.currentPlayerIndex].score =
          model.players[model.currentPlayerIndex].score;
      model.currentRoundScore = 0;
      model.dartCount = 0;
      switchPlayer();
      return;
    }

    addToHistory(newScore, segmentMultiplier, remainingScore);
    addLastDart(newScore, segmentMultiplier);

    if (model.dartHistory.length >= 3 &&
        model.dartHistory
            .sublist(model.dartHistory.length - 3)
            .every((dart) => dart['score'] == 20 && dart['multiplier'] == 3)) {
      play180Sound();
    }

    if (remainingScore == 0) {
      model.players[model.currentPlayerIndex].legsWon++;
      if (model.players[model.currentPlayerIndex].legsWon >= model.legsPerSet) {
        model.players[model.currentPlayerIndex].setsWon++;
        for (var player in model.players) {
          player.legsWon = 0;
        }

        if (model.players[model.currentPlayerIndex].setsWon >=
            model.setsToWin) {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text(
                  "${model.players[model.currentPlayerIndex].name} gewinnt das Spiel!"),
              content: const Text("Herzlichen Glückwunsch!"),
              actions: <Widget>[
                TextButton(
                  child: const Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                    resetGame();
                  },
                ),
              ],
            ),
          );
          return;
        } else {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text(
                  "${model.players[model.currentPlayerIndex].name} gewinnt den Satz!"),
              content: const Text("Nächster Satz beginnt."),
              actions: <Widget>[
                TextButton(
                  child: const Text('OK'),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ],
            ),
          );
        }
      } else {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text(
                "${model.players[model.currentPlayerIndex].name} gewinnt das Leg!"),
            content: const Text("Nächstes Leg beginnt."),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        );
      }
      resetLeg();
      return;
    } else if (remainingScore > 0) {
      model.players[model.currentPlayerIndex].score = remainingScore;
      model.currentRoundScore += newScore * segmentMultiplier;
    } else {
      model.players[model.currentPlayerIndex].score =
          model.players[model.currentPlayerIndex].score;
      model.dartCount = 0;
      switchPlayer();
      return;
    }

    model.dartCount++;
    if (model.dartCount >= 3) {
      switchPlayer();
    }
  }

  void addToHistory(int score, int multiplier, int remainingScore) {
    model.dartHistory.add({
      'player': model.currentPlayerIndex,
      'score': score,
      'multiplier': multiplier,
      'roundScore': model.currentRoundScore + score * multiplier,
      'remainingScore': remainingScore,
      'round': model.currentRound,
      'dartCount': model.dartCount
    });
  }

  void addLastDart(int score, int multiplier) {
    model.lastDarts.add({
      'player': model.currentPlayerIndex,
      'score': score,
      'multiplier': multiplier,
    });
  }

  void switchPlayer() {
    model.dartCount = 0;
    model.currentRoundScore = 0;
    model.currentPlayerIndex =
        (model.currentPlayerIndex + 1) % model.players.length;
    if (model.currentPlayerIndex == 0) {
      model.currentRound++;
    }
    model.players[model.currentPlayerIndex].score =
        model.players[model.currentPlayerIndex].score;
  }

  void undoLastDart() {
    if (model.dartHistory.isNotEmpty) {
      var lastDart = model.dartHistory.removeLast();
      int lastPlayer = lastDart['player']!;
      int scoreToAddBack = lastDart['score']! * lastDart['multiplier']!;
      model.players[lastPlayer].score =
          lastDart['remainingScore']! + scoreToAddBack;
      model.currentRoundScore = lastDart['roundScore']! - scoreToAddBack;
      model.currentPlayerIndex = lastPlayer;
      model.currentRound = lastDart['round']!;
      model.dartCount = lastDart['dartCount']!;

      model.lastDarts = model.dartHistory
          .where((dart) =>
              dart['player'] == model.currentPlayerIndex &&
              dart['round'] == model.currentRound)
          .map((dart) => {
                'player': dart['player']!,
                'score': dart['score']!,
                'multiplier': dart['multiplier']!
              })
          .toList();
    }
  }

  void resetLeg() {
    for (var player in model.players) {
      player.score = getScoreForGameMode(model.gameMode);
      player.doubleInAchieved = false;
    }
    model.currentRoundScore = 0;
    model.dartCount = 0;
    model.dartHistory.clear();
    model.lastDarts.clear();
  }

  void resetGame() {
    for (var player in model.players) {
      player.score = getScoreForGameMode(model.gameMode);
      player.setsWon = 0;
      player.legsWon = 0;
      player.doubleInAchieved = false;
    }
    model.currentPlayerIndex = 0;
    model.currentRound = 1;
    model.currentRoundScore = 0;
    model.dartCount = 0;
    model.dartHistory.clear();
    model.lastDarts.clear();
  }

  List<Widget> buildDartScores() {
    return model.lastDarts.map((dart) {
      String label;
      if (dart['multiplier'] == 3) {
        label = 'T${dart['score']}';
      } else if (dart['multiplier'] == 2) {
        label = 'D${dart['score']}';
      } else if (dart['score'] == 50) {
        label = 'DBull';
      } else {
        label = '${dart['score']}';
      }
      return Container(
        margin: const EdgeInsets.symmetric(horizontal: 2),
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.red),
          borderRadius: BorderRadius.circular(4),
        ),
        child: Text(
          label,
          style: const TextStyle(
            fontFamily: 'Digital-7',
            color: Colors.red,
            fontSize: 35,
          ),
        ),
      );
    }).toList();
  }

  List<String> getCheckoutWays(int score) {
    return doubleOutCheckouts[score] ?? [];
  }

  Widget buildCheckoutWays() {
    int remainingScore = model.players[model.currentPlayerIndex].score;
    List<String> checkouts = getCheckoutWays(remainingScore);
    if (checkouts.isEmpty) {
      return Container();
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        const Text(
          'Checkout Wege:',
          style: TextStyle(
            fontFamily: 'Digital-7',
            color: Colors.red,
            fontSize: 35,
          ),
        ),
        const SizedBox(width: 50),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: checkouts.map((checkout) {
            return Container(
              margin: const EdgeInsets.symmetric(horizontal: 2),
              padding: const EdgeInsets.all(4),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.yellow),
                borderRadius: BorderRadius.circular(4),
              ),
              child: Text(
                checkout,
                style: const TextStyle(
                  fontFamily: 'Digital-7',
                  color: Colors.yellow,
                  fontSize: 35,
                ),
              ),
            );
          }).toList(),
        ),
      ],
    );
  }

  void onNumberPressed(int number, context) {
    if (multiplier == 1 && number == 25) {
      multiplier = 2;
    }
    updateScore(number, multiplier, context);
    multiplier = 1;
  }

  ButtonStyle _keyboardButtonStyle(
      {required Color borderColor, required Color textColor}) {
    return ElevatedButton.styleFrom(
      foregroundColor: textColor,
      backgroundColor: Colors.transparent,
      side: BorderSide(color: borderColor),
      padding: const EdgeInsets.all(8),
      textStyle: const TextStyle(fontSize: 18),
    );
  }

  Widget buildKeyboard(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(10),
      child: GridView.count(
        crossAxisCount: 5,
        shrinkWrap: true,
        children: List.generate(20, (index) {
          int number = index + 1;
          return ElevatedButton(
            onPressed: () => onNumberPressed(number, context),
            style: _keyboardButtonStyle(
                borderColor: Colors.red, textColor: Colors.red),
            child: Text(number.toString()),
          );
        })
          ..addAll([
            ElevatedButton(
              onPressed: () => onNumberPressed(25, context),
              style: _keyboardButtonStyle(
                  borderColor: Colors.green, textColor: Colors.green),
              child: const Text('Bull'),
            ),
            ElevatedButton(
              onPressed: () => onNumberPressed(50, context),
              style: _keyboardButtonStyle(
                  borderColor: Colors.red, textColor: Colors.red),
              child: const Text('Bullseye'),
            ),
            ElevatedButton(
              onPressed: () => multiplier = 2,
              style: _keyboardButtonStyle(
                  borderColor: Colors.red, textColor: Colors.red),
              child: const Text('Double'),
            ),
            ElevatedButton(
              onPressed: () => multiplier = 3,
              style: _keyboardButtonStyle(
                  borderColor: Colors.red, textColor: Colors.red),
              child: const Text('Triple'),
            ),
          ]),
      ),
    );
  }
}
