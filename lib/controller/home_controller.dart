// controller/home_controller.dart
import 'dart:math';
import 'package:Darts_Counter/controller/main_controller.dart';
import 'package:Darts_Counter/model/main_model.dart';
import 'package:Darts_Counter/model/player.dart';
import 'package:Darts_Counter/screens/main_view.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../model/home_model.dart';

class HomeController {
  final HomeModel model = HomeModel();
  final TextEditingController playerController = TextEditingController();
  final TextEditingController profileController = TextEditingController();

  List<String> gameModes = ['301', '501', '701'];
  List<String> gameSettings = [
    'Straight Out',
    'Double In',
    'Double Out',
    'Double In Double Out',
    'Master Out'
  ];

  int getScoreForGameMode(String gameMode) {
    switch (gameMode) {
      case '501':
        return 501;
      case '301':
        return 301;
      case '701':
        return 701;
      default:
        return 0; // Fallback, falls ein ungültiger Modus ausgewählt ist
    }
  }

  Future<void> loadInitialData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    model.playerProfiles = prefs.getStringList('playerProfiles') ?? [];
  }

  Future<void> _savePlayerProfiles() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList('playerProfiles', model.playerProfiles);
  }

  void addPlayer(String name) {
    if (model.players.length < 4) {
      if (name.isEmpty) {
        name = _generateGuestPlayerName();
      }
      model.players.add(name);
    }
  }

  void removePlayer(int index) {
    model.players.removeAt(index);
  }

  void addPlayerProfile(String name) {
    if (name.isNotEmpty && !model.playerProfiles.contains(name)) {
      model.playerProfiles.add(name);
      _savePlayerProfiles();
    }
  }

  void removePlayerProfile(int index, BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text('Wirklich löschen?'),
        content:
            const Text('Möchten Sie dieses Spielerprofil wirklich löschen?'),
        actions: <Widget>[
          TextButton(
            child: const Text('Abbrechen'),
            onPressed: () => Navigator.of(context).pop(),
          ),
          TextButton(
            child: const Text('Löschen'),
            onPressed: () {
              model.playerProfiles.removeAt(index);
              _savePlayerProfiles();
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  String _generateGuestPlayerName() {
    int guestNumber = 1;
    String guestName;
    do {
      guestName = 'Gastspieler $guestNumber';
      guestNumber++;
    } while (model.players.contains(guestName));
    return guestName;
  }

  void startGame(BuildContext context) {
    List<String> gamePlayers = List.from(model.players);
    if (model.randomOrder) {
      gamePlayers.shuffle(Random());
    }

    if (model.players.isNotEmpty) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) {
            List<Player> gamePlayers = model.players
                .map((player) => Player(
                      name: player,
                      score: getScoreForGameMode(model.selectedGameMode),
                    ))
                .toList();
            MainModel mainModel = MainModel(
              players: gamePlayers,
              gameMode: model.selectedGameMode,
              gameSetting: model.selectedGameSetting,
              setsToWin: model.setsToWin,
              legsPerSet: model.legsPerSet,
            );
            MainController mainController = MainController(mainModel);
            return MainView(controller: mainController);
          },
        ),
      );
    } else {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text('Fehler'),
          content: const Text('Fügen Sie mindestens einen Spieler hinzu.'),
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        ),
      );
    }
  }

  void reorderPlayers(int oldIndex, int newIndex) {
    if (newIndex > oldIndex) {
      newIndex -= 1;
    }
    final player = model.players.removeAt(oldIndex);
    model.players.insert(newIndex, player);
  }
}
