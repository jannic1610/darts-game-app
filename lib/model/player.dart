// model/main_model.dart

class Player {
  String name;
  int score;
  int setsWon;
  int legsWon;
  bool doubleInAchieved;

  Player({
    required this.name,
    required this.score,
    this.setsWon = 0,
    this.legsWon = 0,
    this.doubleInAchieved = false,
  });
}
