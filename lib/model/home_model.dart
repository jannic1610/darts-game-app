class HomeModel {
  List<String> players = [];
  List<String> playerProfiles = [];
  String selectedGameMode = '501';
  String selectedGameSetting = 'Straight Out';
  int setsToWin = 1;
  int legsPerSet = 1;
  bool randomOrder = false;
}
