import 'package:Darts_Counter/model/player.dart';

class MainModel {
  List<Player> players;
  String gameMode;
  String gameSetting;
  int setsToWin;
  int legsPerSet;
  int currentPlayerIndex = 0;
  int currentRound = 1;
  int currentRoundScore = 0;
  int dartCount = 0;
  List<Map<String, int>> dartHistory = [];
  List<Map<String, int>> lastDarts = [];

  MainModel({
    required this.players,
    required this.gameMode,
    required this.gameSetting,
    required this.setsToWin,
    required this.legsPerSet,
  });
}
