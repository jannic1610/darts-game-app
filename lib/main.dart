import 'package:Darts_Counter/controller/home_controller.dart';
import 'package:flutter/material.dart';
import 'screens/home_view.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Darts Game',
        theme: ThemeData(
          primarySwatch: Colors.red,
        ),
        home: HomeView(
          HomeController(),
        ));
  }
}
